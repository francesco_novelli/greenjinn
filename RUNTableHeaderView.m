//
//  RUNTableHeaderView.m
//  tally
//
//  Created by Francesco Novelli on 18/06/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNTableHeaderView.h"

#import <UIColor-HexString/UIColor+HexString.h>

@implementation RUNTableHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 200, 40)];
        
        _titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.f];
        _titleLabel.textColor = [UIColor whiteColor];
        
        self.backgroundColor = [UIColor colorWithHexString:@"facf1a"];
        
        _userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 25, 40, 40)];
        
        CALayer * l = [_userImageView layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:20.0];
        
        [self addSubview:_titleLabel];
        [self addSubview:_userImageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

//
//  RUNTableHeaderView.h
//  tally
//
//  Created by Francesco Novelli on 18/06/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUNTableHeaderView : UIView

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UIImageView *userImageView;

@end

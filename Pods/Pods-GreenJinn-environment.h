
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// KLCPopup
#define COCOAPODS_POD_AVAILABLE_KLCPopup
#define COCOAPODS_VERSION_MAJOR_KLCPopup 1
#define COCOAPODS_VERSION_MINOR_KLCPopup 0
#define COCOAPODS_VERSION_PATCH_KLCPopup 0

// MFSideMenu
#define COCOAPODS_POD_AVAILABLE_MFSideMenu
#define COCOAPODS_VERSION_MAJOR_MFSideMenu 0
#define COCOAPODS_VERSION_MINOR_MFSideMenu 5
#define COCOAPODS_VERSION_PATCH_MFSideMenu 4

// UIColor-HexString
#define COCOAPODS_POD_AVAILABLE_UIColor_HexString
#define COCOAPODS_VERSION_MAJOR_UIColor_HexString 1
#define COCOAPODS_VERSION_MINOR_UIColor_HexString 1
#define COCOAPODS_VERSION_PATCH_UIColor_HexString 0


//
//  RUNCouponsTableViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNCouponsTableViewController.h"

#import "RUNItemModel.h"

#import <KLCPopup.h>

#import <UIColor+HexString.h>
#import <MFSideMenu.h>
@import CoreLocation;

@interface RUNCouponsTableViewController () <CLLocationManagerDelegate>  {
    NSArray *items;
    UIView *cartView;
    CLLocationManager *_locationManager;
    CLBeacon *cBeacon;
    KLCPopup* popup;
    BOOL closed;
}

@end

@implementation RUNCouponsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    closed = NO;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self.menuContainerViewController action:@selector(toggleLeftSideMenu)];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cart" style:UIBarButtonItemStylePlain target:self action:@selector(completeCart:)];
    
    items  = [NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist];
    self.title = @"Coupons";
    self.tableView.rowHeight = 200.f;
    
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    CLBeaconRegion *region = nil;
    NSUUID *_uuid = [[NSUUID alloc] initWithUUIDString:@"25666662-3323-23D3-2323-23D232D323D2"];
    region = [[CLBeaconRegion alloc] initWithProximityUUID:_uuid identifier:@"it.runcode.xxxxxx"];
    
    [_locationManager startRangingBeaconsInRegion:region];

}




- (void)completeCart:(id)sender {
    cartView = [[UIView alloc] init];
    cartView.backgroundColor = [UIColor colorWithHexString:@"facf1a"];
    cartView.frame = CGRectMake(0.0, 0.0, 300.0, 300.0);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 280, 30)];
    int Price = 0;
    Price = 34;
    label.text = [@"Cart" stringByAppendingFormat:@" - TOTAL %i.60$", Price];
    label.font = [UIFont systemFontOfSize:18.f];
    label.textAlignment = NSTextAlignmentCenter;
    [cartView addSubview:label];
    
    
    UILabel *nearLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 280, 30)];
    nearLbl.text = @"Go nearby the receiver....";
    nearLbl.tag = 10001;
    [cartView addSubview:nearLbl];
    
    if (items.count > 0) {
        UILabel *pr1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 280, 30)];
        int dis = 1;//rand() % 10;
        Price = Price - dis;
        pr1.text = [@"Discount 1........" stringByAppendingFormat:@"0.75$"];
        [cartView addSubview:pr1];
    }
    if (items.count > 1) {
        UILabel *pr1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 130, 280, 30)];
//        int dis = rand() % 10;
        int dis = 1;
        Price = Price - dis;
        pr1.text = [@"Discount 2........" stringByAppendingFormat:@"%i.00$", dis];
        [cartView addSubview:pr1];
    }
    if (items.count > 2) {
        UILabel *pr1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 160, 280, 30)];
//        int dis = rand() % 10;
        int dis = 2;
        Price = Price - dis;
        pr1.text = [@"Discount 3........" stringByAppendingFormat:@"%i.00$", dis];
        [cartView addSubview:pr1];
    }
    if (items.count > 3) {
        UILabel *pr1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 190, 280, 30)];
//        int dis = rand() % 10;
        int dis = 1;
        Price = Price - dis;
        pr1.text = [@"Discount 4........" stringByAppendingFormat:@"%i.00$", dis];
        [cartView addSubview:pr1];
    }
    
    UILabel *sublabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 250, 280, 30)];
    
    sublabel.text = [@"Discounted Price" stringByAppendingFormat:@" %i.85", Price];
    sublabel.font = [UIFont systemFontOfSize:18.f];
    sublabel.textAlignment = NSTextAlignmentCenter;
    [cartView addSubview:sublabel];
    
    

    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Pay the check" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(closeCartForever:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 250, 300, 50)];
    [button setTag:10002];
    [button setEnabled:NO];
//    [cartView addSubview:button];
    
    popup = [KLCPopup popupWithContentView:cartView];
    [popup show];

    
    if (cBeacon) {
        if (cBeacon.proximity == CLProximityImmediate) {
            nearLbl.hidden = YES;
        } else {
            nearLbl.hidden = NO;
        }
    } else {
        nearLbl.hidden = NO;
    }
    
    
}


- (void)closeCartForever:(id)sender {
    [popup dismiss:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Check Paid" message:@"Thanks. See you soon..!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:kRUNCouponPlist];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    items  = [NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist];
    
    [self.tableView reloadData];
}

- (void)checkIfCanDestroyCart {
    if (closed) {
        
        return;
    }
    
    if (!popup) {
        return;
    }
    
    UILabel *nearLbl = [cartView viewWithTag:10001];
    UIButton *btn = [cartView viewWithTag:10002];
    if (cBeacon) {
        if (cBeacon.proximity == CLProximityImmediate) {
            nearLbl.hidden = YES;
            btn.enabled = YES;
            if (popup.isShowing) {
                [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(closeCartForever:) userInfo:nil repeats:NO];
                closed = YES;
            }

        } else {
            nearLbl.hidden = NO;
            btn.enabled = NO;
        }
    } else {
        nearLbl.hidden = NO;
        btn.enabled = NO;
    }
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"REGION %@", region);
//    if ([region isKindOfClass:[CLBeaconRegion class]]) {
//        CLBeaconRegion *beacon = (CLBeaconRegion *)region;
//        [locationManager startRangingBeaconsInRegion:beacon];
//    }
    
}
- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    NSLog(@"%@ - %i", region, state);
}
-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    CLBeacon *beacon = [[CLBeacon alloc] init];
    beacon = [beacons lastObject];
    cBeacon = beacon;
    NSLog(@"%@, %@", beacon.major, beacon.minor);

    [self checkIfCanDestroyCart];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"Couldn't turn on ranging: Location services are not enabled.");
    }
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        NSLog(@"Couldn't turn on monitoring: Location services not authorised.");
    }
}
- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
    NSLog(@"Failed monitoring region: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location manager failed: %@", error);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"reuseIdentifierRUNLeftMenuTableViewController";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setFrame:CGRectMake(0, 0, 50, 50)];
        [button setTitle:@"Delete" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(deleteRow:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = button;
    }
    // Configure the cell...
    
    cell.accessoryView.tag = indexPath.row;
    cell.imageView.image =  [UIImage imageNamed:[NSString stringWithFormat:@"dis%@.png", items[indexPath.row]]];
    
    
    return cell;
}

- (void)deleteRow:(UIButton *)btn {
    NSMutableArray *tmp = @[].mutableCopy;
    for (NSNumber *number in items) {
        
        if (number.integerValue != [items[btn.tag] integerValue]) {
            [tmp addObject:number];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tmp forKey:kRUNCouponPlist];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    items  = [NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist];
    
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSMutableArray *tmp = @[].mutableCopy;
        for (NSNumber *number in items) {
            
            if (number.integerValue != [items[indexPath.row] integerValue]) {
                [tmp addObject:number];
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:tmp forKey:kRUNCouponPlist];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        items  = [NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist];
        
        [self.tableView reloadData];
        
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

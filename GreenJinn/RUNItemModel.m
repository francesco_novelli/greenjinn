//
//  RUNItemModel.m
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNItemModel.h"

NSString *const kRUNItemModelPlist = @"kRUNItemModelPlist";

NSString *const kRUNCouponPlist = @"kRUNCouponPlist";

@implementation RUNItemModel

+ (NSArray *)itemList {
    
    NSArray *items = [[NSUserDefaults standardUserDefaults]
                      objectForKey:kRUNItemModelPlist];
    
    NSMutableArray *r = @[].mutableCopy;
    for (NSDictionary *dic  in items) {
        RUNItemModel *item = [RUNItemModel new];
        
        item.title = dic[@"title"];
        item.desc = dic[@"desc"];
        item.checked = [dic[@"checked"] boolValue];
       
        [r addObject:item];
    }
    
    return r;
}

+ (NSArray *)itemListAsDic {
    
    NSArray *items = [[NSUserDefaults standardUserDefaults]
                      objectForKey:kRUNItemModelPlist];
    
    
    
    return items;
}
- (void)save {
    NSMutableArray *array = [NSMutableArray arrayWithArray:[RUNItemModel itemListAsDic]];
    
    [array addObject:[self dicRappresentation]];
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:kRUNItemModelPlist];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)deleteItem {
    NSMutableArray *array = [RUNItemModel itemListAsDic].mutableCopy;
    NSMutableArray *tmp = [NSMutableArray array];
    
    for (NSDictionary *dic in array) {
       
        NSString *title = self.title;
        NSString *dicTit = dic[@"title"];
        
        if (![title isEqualToString:dicTit]) {// && [dic[@"desc"] isEqualToString:self.desc]) {
            [tmp addObject:dic];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:tmp forKey:kRUNItemModelPlist];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (NSDictionary *)dicRappresentation {
    return @{@"title": self.title, @"desc" : self.desc, @"checked" : @(self.checked)};
}
@end

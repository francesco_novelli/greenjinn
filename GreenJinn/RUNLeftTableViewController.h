//
//  RUNLeftTableViewController.h
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Accounts;
@import Social;
@interface RUNLeftTableViewController : UITableViewController

@property (nonatomic, retain) ACAccountStore *accountStore;
@property (nonatomic, retain) ACAccount *facebookAccount;

- (IBAction)logWithFB:(id)sender;

@end

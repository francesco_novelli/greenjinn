//
//  RUNWishListViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 08/08/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNWishListViewController.h"

#import "RUNItemModel.h"

#import <MFSideMenu.h>

@interface RUNWishListViewController () {
    int img1Number;
    int img2Number;
}

@end

@implementation RUNWishListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Wish List";
    
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self.menuContainerViewController action:@selector(toggleLeftSideMenu)];
    
    NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
    
    if ([std objectForKey:@"txt1"]) {
        self.txt1.text = [std objectForKey:@"txt1"];
    }
    if ([std objectForKey:@"txt2"]) {
        self.txt2.text = [std objectForKey:@"txt2"];
    }
    if ([std objectForKey:@"txt3"]) {
        self.txt3.text = [std objectForKey:@"txt3"];
    }
    
    
//    [self changeIMG];
//    [NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(changeIMG) userInfo:nil repeats:YES];
//    
//    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDiscount:)];
    tapGesture.numberOfTapsRequired = 1;
    [_img1 addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDiscount:)];
    tapGesture2.numberOfTapsRequired = 1;
    [_img2 addGestureRecognizer:tapGesture2];
    
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDiscount:)];
    tapGesture3.numberOfTapsRequired = 1;
    [_img3 addGestureRecognizer:tapGesture3];
//    // Do any additional setup after loading the view from its nib.
}

- (void)addDiscount:(UITapGestureRecognizer *)s {
    NSInteger img = [(UIImageView *)s.view tag];
    int toAdd = 0;
    
    if (img == 101) {
        
        toAdd = 14;
        
    }
    if (img == 102) {
        
        toAdd = 15;
    }
    if (img == 103) {
        
        toAdd = 16;
    }
    
    NSMutableArray *coupons = [[NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist] mutableCopy];
    if (!coupons) {
        coupons = @[].mutableCopy;
    }
    for (id number in coupons) {
        if ([number integerValue] == toAdd) {
            NSLog(@"Esiste");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already this coupon on your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return;
            return;
        }
    }
    
//    if (coupons.count > 3) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already 4 coupons" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//        
//        return;
//    }
    
    [coupons addObject:@(toAdd)];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"!!" message:@"coupon added to your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [NSUserDefaults.standardUserDefaults setObject:coupons forKey:kRUNCouponPlist];
    [NSUserDefaults.standardUserDefaults synchronize];
}

- (void)changeIMG {
    img1Number = rand() % 16;
    
    _img1.image = [UIImage imageNamed:[NSString stringWithFormat:@"dis%i.png", img1Number+1]];
    do {
        img2Number = rand() % 16;
    } while (img1Number == img2Number);
    
    _img2.image = [UIImage imageNamed:[NSString stringWithFormat:@"dis%i.png", img2Number+1]];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
        NSUserDefaults *std = [NSUserDefaults standardUserDefaults];
    
    if (textField.tag == 6001) {
        [std setObject:textField.text forKey:@"txt1"];
    }
    if (textField.tag == 6002) {
        [std setObject:textField.text forKey:@"txt2"];
    }
    if (textField.tag == 6003) {
        [std setObject:textField.text forKey:@"txt3"];
    }
    
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

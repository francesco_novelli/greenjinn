//
//  RUNItemModel.h
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kRUNCouponPlist;

@interface RUNItemModel : NSObject


@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, readwrite) BOOL checked;

+ (NSArray *)itemList;
- (void)save;
- (void)deleteItem;

@end

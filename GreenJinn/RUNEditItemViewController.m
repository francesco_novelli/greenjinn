//
//  RUNEditItemViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNEditItemViewController.h"

#import "RUNItemModel.h"

@interface RUNEditItemViewController ()

@end

@implementation RUNEditItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveItem:(id)sender {
    RUNItemModel *model = [[RUNItemModel alloc] init];
    model.title = self.titleField.text;
    model.desc = self.descField.text;
    
    [model save];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end

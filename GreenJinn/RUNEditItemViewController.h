//
//  RUNEditItemViewController.h
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RUNItemModel;

@interface RUNEditItemViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *descField;
- (IBAction)saveItem:(id)sender;


@property (nonatomic, retain) RUNItemModel *model;

@end

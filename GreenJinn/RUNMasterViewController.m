//
//  RUNMasterViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNMasterViewController.h"

#import "RUNDetailViewController.h"

#import "RUNEditItemViewController.h"

#import "RUNItemModel.h"


#import <MFSideMenu.h>

@interface RUNMasterViewController () <UIAlertViewDelegate> {
    NSMutableArray *_objects;
    int img1Number;
    int img2Number;
    int selectedIndex;
    UISearchBar *searchBar;
    BOOL isSearching;
    NSMutableArray *filteredProducts;
}
@end

@implementation RUNMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self.menuContainerViewController action:@selector(toggleLeftSideMenu)];

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    [self changeIMG];
    [NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(changeIMG) userInfo:nil repeats:YES];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDiscount:)];
    tapGesture.numberOfTapsRequired = 1;
    [_img1 addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addDiscount:)];
    tapGesture2.numberOfTapsRequired = 1;
    [_img2 addGestureRecognizer:tapGesture2];
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    lpgr.delegate = self;
    [self.tableView addGestureRecognizer:lpgr];
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];

    searchBar.delegate = self;

    self.tableView.tableHeaderView = searchBar;

    filteredProducts = [[NSMutableArray alloc] init];

    isSearching = NO;
    
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //    NSLog(@"Search Text %@", searchText);
    [filteredProducts removeAllObjects];
    isSearching = YES;
    for (RUNItemModel *item in _objects) {
        if ([item.title rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [filteredProducts addObject:item];
        }
    }
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar {
    [_searchBar setShowsCancelButton:YES animated:YES];
    
    isSearching = YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar {
    _searchBar.text = @"";
    
    [_searchBar setShowsCancelButton:NO animated:YES];
    [_searchBar resignFirstResponder];
    
    isSearching = NO;
    [self.tableView reloadData];
}

- (IBAction)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint p = [gestureRecognizer locationInView:self.tableView];
        
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        if (indexPath == nil) {
            NSLog(@"long press on table view but not on a row");
        } else {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            if (cell.isHighlighted) {
                NSLog(@"long press on table view at section %d row %d", indexPath.section, indexPath.row);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Item" message:nil delegate:self cancelButtonTitle:@"Leave" otherButtonTitles:@"Delete", @"Edit", nil];
                [alert setTag:999];
                
                [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
                [[alert textFieldAtIndex: 0] setText:[_objects[indexPath.row] title]];
                [alert show];
                selectedIndex = indexPath.row;
            }
        }
    }
}

- (void)addDiscount:(UITapGestureRecognizer *)s {
    NSInteger img = [(UIImageView *)s.view tag];
    int toAdd = 0;
    
    if (img == 101) {
        
        toAdd = img1Number + 1;
        
    }
    if (img == 102) {

        toAdd = img2Number + 1;
    }
    
    NSMutableArray *coupons = [[NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist] mutableCopy];
    if (!coupons) {
        coupons = @[].mutableCopy;
    }
    for (id number in coupons) {
        if ([number integerValue] == toAdd) {
            NSLog(@"Esiste");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already this coupon on your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return;
            return;
        }
    }
//    
//    if (coupons.count > 3) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already 4 coupons" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//        
//        return;
//    }
    
    [coupons addObject:@(toAdd)];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"!!" message:@"coupon added to your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [NSUserDefaults.standardUserDefaults setObject:coupons forKey:kRUNCouponPlist];
    [NSUserDefaults.standardUserDefaults synchronize];
}

- (void)changeIMG {
    img1Number = rand() % 13;
    
    _img1.image = [UIImage imageNamed:[NSString stringWithFormat:@"dis%i.png", img1Number+1]];
    do {
        img2Number = rand() % 13;
    } while (img1Number == img2Number);
    
    _img2.image = [UIImage imageNamed:[NSString stringWithFormat:@"dis%i.png", img2Number+1]];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _objects = [RUNItemModel itemList].mutableCopy;
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
//    RUNEditItemViewController *vc = [[RUNEditItemViewController alloc] initWithNibName:@"RUNEditItemViewController" bundle:nil];
//    
//    [self.navigationController pushViewController:vc animated:YES];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Item" message:nil delegate:self cancelButtonTitle:@"Insert" otherButtonTitles:@"Leave", nil];
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //    NSLog(@"ALert %@", alertView.textInputContextIdentifier);
    
    if (alertView.tag == 999) {
        if (buttonIndex == 0) {
            return;
        }
        [_objects[selectedIndex] deleteItem];
        
        if (buttonIndex == 2) {
            RUNItemModel *model = [[RUNItemModel alloc] init];
            model.title = [[alertView textFieldAtIndex: 0] text];
            model.desc = @"xx";
            [model save];
        }
        
        _objects = [RUNItemModel itemList].mutableCopy;
        
        [self.tableView reloadData];
        
        return;
    }
    
    if (buttonIndex == 0) {
        
        
        RUNItemModel *model = [[RUNItemModel alloc] init];
        model.title = [[alertView textFieldAtIndex: 0] text];
        model.desc = @"xx";
        [model save];
        
        _objects = [RUNItemModel itemList].mutableCopy;
        
        [self.tableView reloadData];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (isSearching) {
        return filteredProducts.count;
    }
    
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    RUNItemModel *object = nil;
    if (isSearching) {
        object = filteredProducts[indexPath.row];
    } else {
        object = _objects[indexPath.row];
    }
    cell.textLabel.text = [object title];
    
    if (object.checked) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    RUNItemModel *object = nil;
    if (isSearching) {
        object = filteredProducts[indexPath.row];
    } else {
        object = _objects[indexPath.row];
    }
    object.checked = !object.checked;
    
    [object deleteItem];
    [object save];
    
    [self.tableView reloadData];
    
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    return NO;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        
//        [_objects[indexPath.row] deleteItem];
//        
//        _objects = [RUNItemModel itemList].mutableCopy;
//        
//        [self.tableView reloadData];
//        
//        
////        
////        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//    }
//}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end

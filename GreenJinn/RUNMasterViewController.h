//
//  RUNMasterViewController.h
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUNMasterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;

@end

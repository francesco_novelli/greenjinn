//
//  RUNLeftTableViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNLeftTableViewController.h"

#import "RUNTableHeaderView.h"

#import "RUNCouponsTableViewController.h"

#import <MFSideMenu.h>


#import "RUNMasterViewController.h"


#import "RUNListCouponsTableViewController.h"


#import "RUNWishListViewController.h"

@interface RUNLeftTableViewController () {
    NSArray *items;
    BOOL isFacebookAvailable;
    RUNTableHeaderView *viewHeader;
}

@end

@implementation RUNLeftTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    items = @[@"Shopping List", @"Wish List", @"Selected Coupons", @"Coupons Archive"];
    
    
    if(!_accountStore)
        _accountStore = [[ACAccountStore alloc] init];
    
    
    
    viewHeader = [[RUNTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, 300, 75)];
    viewHeader.titleLabel.text = [@"Guest" uppercaseString];
    
    viewHeader.userImageView.image = [UIImage imageNamed:@"default_user"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setFrame:CGRectMake(200, 30, 80, 30)];
    [button setTitle:@"LogIn" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(logWithFB:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTag:100001];
    
    [viewHeader addSubview:button];
    

    self.tableView.tableHeaderView = viewHeader;
    
    
    ACAccountType *facebookTypeAccount = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    if (facebookTypeAccount != nil) {
        [self logWithFB:nil];
    }
}

- (IBAction)logWithFB:(id)sender {
    
    if (isFacebookAvailable) {
        isFacebookAvailable = NO;
        if (self.facebookAccount) {
                [self.accountStore removeAccount:self.facebookAccount withCompletionHandler:^(BOOL success, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(),^{
                        
                        UIButton *btn = (UIButton *)[viewHeader viewWithTag:100001];
                        [btn setTitle:@"LogIN" forState:UIControlStateNormal];
                        
                        viewHeader.titleLabel.text = @"GUEST";
                        viewHeader.userImageView.image = [UIImage imageNamed:@"default_user"];
                    });
                } ];
        }
        return;
        
    }
    
    self.accountStore = [[ACAccountStore alloc]init];
    ACAccountType *FBaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSString *key = @"844820345543733";
    NSDictionary *dictFB = @{ACFacebookAppIdKey : key,
                             ACFacebookPermissionsKey : @[@"email"]
                             
                             };
    
    [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *e) {
         if (granted)
         {
             NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
             //it will always be the last object with single sign on
             self.facebookAccount = [accounts lastObject];
             
             
             
             [self get];
             
             
             isFacebookAvailable = 1;
         } else
         {
             isFacebookAvailable = 0;
             
         }
     }];
    
    
    
}

-(void)checkfacebookstatus
{
    if (isFacebookAvailable == 0)
    {
        //        [self checkFacebook];
        isFacebookAvailable = 1;
    }
    else
    {
        printf("Get out from our game");
    }
}

-(void)accountChanged:(NSNotification *)notification
{
    [self attemptRenewCredentials];
}

-(void)attemptRenewCredentials
{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    NSLog(@"Good to go");
                    [self get];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    NSLog(@"User declined permission");
                    break;
                case ACAccountCredentialRenewResultFailed:
                    NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    break;
                default:
                    break;
            }
            
        }
        else{
            //handle error gracefully
            NSLog(@"error from renew credentials%@",error);
        }
    }];
}
-(void)get
{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
    request.account = self.facebookAccount;
    
    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        
        if(!error)
        {
            
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSString *  fbname = [NSString stringWithFormat:@"%@",[list objectForKey:@"name"]];

            if([list objectForKey:@"error"]!=nil)
            {
                [self attemptRenewCredentials];
            }
            dispatch_async(dispatch_get_main_queue(),^{
                
                UIButton *btn = (UIButton *)[viewHeader viewWithTag:100001];
                [btn setTitle:@"LogOut" forState:UIControlStateNormal];
                
                viewHeader.titleLabel.text = fbname;
                viewHeader.userImageView.image = [UIImage imageNamed:@"photoprofile.jpeg"];
            });
        }
        else
        {
            //handle error gracefully
            NSLog(@"error from get%@",error);
            //attempt to revalidate credentials
        }
        
    }];
//    requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me/picture"];
//    
//    request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
//    request.account = self.facebookAccount;
//    
//    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
//        
//        if(!error)
//        {
//            
//            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//            
//            NSString *  fbname = [NSString stringWithFormat:@"%@",[list objectForKey:@"name"]];
//            
//            if([list objectForKey:@"error"]!=nil)
//            {
//                [self attemptRenewCredentials];
//            }
//            dispatch_async(dispatch_get_main_queue(),^{
//                viewHeader.titleLabel.text = fbname;
//            });
//        }
//        else
//        {
//            //handle error gracefully
//            NSLog(@"error from get%@",error);
//            //attempt to revalidate credentials
//        }
//        
//    }];
    
//    viewHeader.userImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:pictureUrl]]];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"reuseIdentifierRUNLeftMenuTableViewController";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    
    cell.textLabel.text = items[indexPath.row];
    
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];

        UINavigationController *rootViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"rootNavigationController"];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        navigationController.viewControllers = @[rootViewController.viewControllers[0]];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if (indexPath.row == 1) {
        RUNWishListViewController *vc = [[RUNWishListViewController alloc] init];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        navigationController.viewControllers = @[vc];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if (indexPath.row == 2) {
        RUNCouponsTableViewController *vc = [[RUNCouponsTableViewController alloc] init];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        navigationController.viewControllers = @[vc];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    if (indexPath.row == 3) {
        
        RUNListCouponsTableViewController *vc = [[RUNListCouponsTableViewController alloc] init];
        
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        
        navigationController.viewControllers = @[vc];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  RUNAppDelegate.h
//  GreenJinn
//
//  Created by Francesco Novelli on 18/07/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RUNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

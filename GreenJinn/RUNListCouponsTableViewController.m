//
//  RUNListCouponsTableViewController.m
//  GreenJinn
//
//  Created by Francesco Novelli on 08/08/14.
//  Copyright (c) 2014 RunCode. All rights reserved.
//

#import "RUNListCouponsTableViewController.h"

#import "RUNItemModel.h"

#import <MFSideMenu.h>

@interface RUNListCouponsTableViewController ()

@end

@implementation RUNListCouponsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self.menuContainerViewController action:@selector(toggleLeftSideMenu)];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.title = @"Coupons Archive";
    self.tableView.rowHeight = 200.f;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 13;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"reuseIdentifierRUNLeftMenuTableViewController";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setFrame:CGRectMake(0, 0, 50, 50)];
        [button setTitle:@"Add" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(addItem:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = button;
    }
    // Configure the cell...
    
    cell.accessoryView.tag = indexPath.row;
    cell.imageView.image =  [UIImage imageNamed:[NSString stringWithFormat:@"dis%i.png", indexPath.row+1]];
    
    
    return cell;
}

- (void)addItem:(UIButton *)btn {
    
    int toAdd = btn.tag+1;
    
    NSMutableArray *coupons = [[NSUserDefaults.standardUserDefaults objectForKey:kRUNCouponPlist] mutableCopy];
    if (!coupons) {
        coupons = @[].mutableCopy;
    }
    for (id number in coupons) {
        if ([number integerValue] == toAdd) {
            NSLog(@"Esiste");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already this coupon on your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return;
            return;
        }
    }
    
//    if (coupons.count > 3) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You have already 4 coupons" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
//        
//        return;
//    }
    
    [coupons addObject:@(toAdd)];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"!!" message:@"coupon added to your basket" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [NSUserDefaults.standardUserDefaults setObject:coupons forKey:kRUNCouponPlist];
    [NSUserDefaults.standardUserDefaults synchronize];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
